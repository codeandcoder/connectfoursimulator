package net.codeandcoder.connectfour.board;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import net.codeandcoder.connectfour.board.Board.Square;
import net.codeandcoder.connectfour.utils.RandomUtils;


public class BoardBuilder {

	private Square[][] map;
	
	public BoardBuilder(int columns, int rows) {
		map = new Square[columns][rows];
		
		for (int x = 0; x < map.length; x++) {
			for (int y = 0; y < map[0].length; y++) {
				map[x][y] = Square.Empty;
			}
		}
	}
	
	public void putToken(int column, Square color) throws Exception {
		if (column < 0 || column > map.length) {
			throw new Exception("Column index not valid: " + column);
		}
		
		Square[] squares = map[column];
		for (int i = 0; i < squares.length; i++) {
			Square s = squares[i];
			if (s == Square.Empty) {
				squares[i] = color;
				return;
			}
		}
		
		// If it gets here, it means the column was full, so put the token randomly:
		List<Integer> freeColumns = findFreeColumns();
		
		if (!freeColumns.isEmpty()) {
			Random random = RandomUtils.getInstance().getRandom();
			Integer randomColumn = freeColumns.get(random.nextInt(freeColumns.size()));
			putToken(randomColumn, color);
		}
		
	}
	
	private List<Integer> findFreeColumns() {
		List<Integer> freeColumns = new ArrayList<Integer>();
		for (int j = 0; j < map.length; j++) {
			Square[] squares = map[j];
			for (int i = 0; i < squares.length; i++) {
				Square s = squares[i];
				if (s == Square.Empty) {
					freeColumns.add(j);
					break;
				}
			}
		}
		return freeColumns;
	}
	
	public boolean isFull() {
		return findFreeColumns().isEmpty();
	}
	
	public Board buildBoard() throws Exception {
		return new Board(map);
	}
	
	public int getColumnSize() {
		return map.length;
	}
	
	public int getRowSize() {
		return map[0].length;
	}
	
}
