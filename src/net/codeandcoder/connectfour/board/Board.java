package net.codeandcoder.connectfour.board;

public class Board {

	public static enum Square { Empty, Blue, Red };
	private Column[] columns;
	
	public Board(Square[][] map) {
		columns = new Column[map.length];
		for (int i = 0; i < map.length; i++) {
			Square[] column = map[i];
			
			columns[i] = new Column(column);
		}
	}
	
	public Column getColumn(int index) {
		if (index < 0 || index >= columns.length) {
			return null;
		}
		
		return columns[index];
	}
	
	public static class Column {
		
		private Square[] squares;
		
		public Column(Square[] squares) {
			this.squares = squares;
		}
		
		public Square getSquare(int index) {
			if (index < 0 || index >= squares.length) {
				return null;
			}
			
			return squares[index];
		}
		
		public int getSize() {
			return squares.length;
		}
	}
	
	public int getColumnSize() {
		return columns.length;
	}
	
	public int getRowSize() {
		return columns[0].getSize();
	}
	
}
