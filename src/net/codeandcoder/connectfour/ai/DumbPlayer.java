package net.codeandcoder.connectfour.ai;

import java.util.Random;

import net.codeandcoder.connectfour.board.Board;
import net.codeandcoder.connectfour.board.Board.Square;
import net.codeandcoder.connectfour.interfaces.Player;

public class DumbPlayer implements Player {

	private Random random;
	
	@Override
	public String getPlayerName() {
		return "Dummy Dumb";
	}

	@Override
	public void init() {
		random = new Random();
	}

	@Override
	public int act(Square ownColor, Board board) {
		return random.nextInt(board.getColumnSize());
	}

}
