package net.codeandcoder.connectfour.ai;

import net.codeandcoder.connectfour.board.Board;
import net.codeandcoder.connectfour.board.Board.Square;
import net.codeandcoder.connectfour.interfaces.Player;

public class SecuentialPlayer implements Player {

	private int index;
	
	@Override
	public String getPlayerName() {
		return "Secuential Man";
	}

	@Override
	public void init() {
		index = -1;
	}

	@Override
	public int act(Square ownColor, Board board) {
		index++;
		index = index % board.getColumnSize();
		return index;
	}

}
