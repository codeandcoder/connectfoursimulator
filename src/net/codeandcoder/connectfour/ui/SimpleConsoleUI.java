package net.codeandcoder.connectfour.ui;

import net.codeandcoder.connectfour.board.Board;
import net.codeandcoder.connectfour.board.Board.Square;
import net.codeandcoder.connectfour.interfaces.Player;
import net.codeandcoder.connectfour.interfaces.Simulator;
import net.codeandcoder.connectfour.interfaces.UIListener;

public class SimpleConsoleUI implements UIListener {
	
	@Override
	public void gameStarts(Board board, Player bluePlayer, Player redPlayer) {
		System.out.println("Connect Four Battle:");
		System.out.println(bluePlayer.getPlayerName() + " vs " + redPlayer.getPlayerName());
	} 

	@Override
	public void updateBoard(Board board) {
		System.out.print("[");
		for (int i = board.getRowSize()-1; i >= 0; i--) {
			for (int j = 0; j < board.getColumnSize(); j++) {
				Square color = board.getColumn(j).getSquare(i);
				if (color == Square.Empty) {
					System.out.print("_");
				} else if (color == Square.Blue) {
					System.out.print("X");
				} else {
					System.out.print("O");
				}
				System.out.print("[");
			}
			System.out.println();
			if (i != 0) System.out.print("[");
		}
		System.out.println("---------------------------------------------");
	}

	@Override
	public void gameFinished(Player winner, int moves) {
		if (winner == null) {
			System.out.println("Draw!");
		} else {
			System.out.println(winner.getPlayerName() + " won in " + moves + " moves!");
		}
	}

	@Override
	public void showStats(Simulator.Stats stats) {
		int total = stats.player1Wins + stats.player2Wins + stats.draws;
		int p1Percentatge = (int) (stats.player1Wins * 1.0 / total * 100);
		int p2Percentatge = (int) (stats.player2Wins * 1.0 / total * 100);
		System.out.println(total + " rounds have been simulated.");
		System.out.println(stats.player1.getPlayerName() + " won " + stats.player1Wins + " rounds. (" + p1Percentatge +"%)");
		System.out.println("Used " + stats.player1MovesPerWin + " moves per win. (average)");
		System.out.println(stats.player2.getPlayerName() + " won " + stats.player2Wins + " rounds. (" + p2Percentatge +"%)");
		System.out.println("Used " + stats.player2MovesPerWin + " moves per win. (average)");
	}

}
