package net.codeandcoder.connectfour.interfaces;

public interface Simulator {

	public Result simulateOneGame(Player bluePlayer, Player redPlayer, int columns, int rows);
	public Result simulateOneGame(Player bluePlayer, Player redPlayer);
	public Result simulateOnRound(Player player1, Player player2, int columns, int rows);
	public Result simulateOnRound(Player player1, Player player2);
	public Stats simulateMultipleRounds(Player player1, Player player2, int numberOfRounds);
	
	public static class Result {
		public Player player1;
		public Player player2;
		
		public Player winner;
		public int moves;
	}
	
	public static class Stats {
		public Player player1;
		public Player player2;
		
		public int player1Wins;
		public int player1MovesPerWin;
		
		public int player2Wins;
		public int player2MovesPerWin;
		
		public int draws;
	}
}
