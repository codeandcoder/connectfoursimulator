package net.codeandcoder.connectfour.interfaces;

import net.codeandcoder.connectfour.board.Board;
import net.codeandcoder.connectfour.board.Board.Square;

public interface Player {

	public String getPlayerName();
	
	public void init();
	public int act(Square ownColor, Board board);
	
}
