package net.codeandcoder.connectfour.interfaces;

import net.codeandcoder.connectfour.board.Board;

public interface UIListener {

	public void gameStarts(Board board, Player bluePlayer, Player redPlayer);
	public void updateBoard(Board board);
	public void gameFinished(Player winner, int moves);
	
	public void showStats(Simulator.Stats stats);
	
}
