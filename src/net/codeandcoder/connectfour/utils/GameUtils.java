package net.codeandcoder.connectfour.utils;

import net.codeandcoder.connectfour.board.Board;
import net.codeandcoder.connectfour.board.Board.Column;
import net.codeandcoder.connectfour.board.Board.Square;
import net.codeandcoder.connectfour.interfaces.Player;

public class GameUtils {

	public static Player checkWin(Board board, Player bluePlayer, Player redPlayer) {
		Player winner = null;
		
		winner = GameUtils.checkHorizontal(board, bluePlayer, redPlayer);
		if (winner != null) return winner;
		winner = GameUtils.checkVertical(board, bluePlayer, redPlayer);
		if (winner != null) return winner;
		winner = GameUtils.checkRightToLeftDiagonals(board, bluePlayer, redPlayer);
		if (winner != null) return winner;
		winner = GameUtils.checkLeftToRightDiagonals(board, bluePlayer, redPlayer);
		
		return winner;
	}
	
	public static Player checkVertical(Board board, Player bluePlayer, Player redPlayer) {
		Square lastTokenColor = Square.Empty;
		int connectedCount = 0;
		
		for (int i = 0; i < board.getColumnSize(); i++) {
			Column c = board.getColumn(i);
			for (int j = 0; j < board.getRowSize(); j++) {
				Square color = c.getSquare(j);
				if (color == Square.Empty) {
					connectedCount = 0;
				} else if (lastTokenColor != color) {
					connectedCount = 1;
				} else {
					connectedCount++;
				}
				lastTokenColor = color;
				if (connectedCount == 4) {
					return color == Square.Blue ? bluePlayer : redPlayer;
				}
			}
			lastTokenColor = Square.Empty;
			connectedCount = 0;
		}
		
		return null;
	}
	
	public static Player checkHorizontal(Board board, Player bluePlayer, Player redPlayer) {
		Square lastTokenColor = Square.Empty;
		int connectedCount = 0;
		
		for (int i = 0; i < board.getRowSize(); i++) {
			for (int j = 0; j < board.getColumnSize(); j++) {
				Square color = board.getColumn(j).getSquare(i);
				if (color == Square.Empty) {
					connectedCount = 0;
				} else if (lastTokenColor != color) {
					connectedCount = 1;
				} else {
					connectedCount++;
				}
				lastTokenColor = color;
				if (connectedCount == 4) {
					return color == Square.Blue ? bluePlayer : redPlayer;
				}
			}
			lastTokenColor = Square.Empty;
			connectedCount = 0;
		}
		
		return null;
	}
	
	public static Player checkRightToLeftDiagonals(Board board, Player bluePlayer, Player redPlayer) {
		if (board.getColumnSize() >= 4) {
			Player winner = null;
			
			for (int i = 3; i < board.getColumnSize(); i++) {
				winner = checkRToLDiag(board, bluePlayer, redPlayer, i, board.getRowSize()-1);
				if (winner != null) return winner;
			}
			
			if (board.getRowSize() > 4) {
				for (int i = board.getRowSize()-1; i > 3; i--) {
					winner = checkRToLDiag(board, bluePlayer, redPlayer, board.getColumnSize()-1, i);
					if (winner != null) return winner;
				}
			}
		}
		
		return null;
	}
	
	public static Player checkRToLDiag(Board board, Player bluePlayer, Player redPlayer, int startColumn, int startRow) {
		Square lastTokenColor = Square.Empty;
		int connectedCount = 0;
		
		int i = startRow;
		for (int j = startColumn; j >= 0 && i >= 0; j--) {
			Square color = board.getColumn(j).getSquare(i);
			if (color == Square.Empty) {
				connectedCount = 0;
			} else if (lastTokenColor != color) {
				connectedCount = 1;
			} else {
				connectedCount++;
			}
			lastTokenColor = color;
			if (connectedCount == 4) {
				return color == Square.Blue ? bluePlayer : redPlayer;
			}
			i--;
		}
		
		return null;
	}
	
	public static Player checkLeftToRightDiagonals(Board board, Player bluePlayer, Player redPlayer) {
		if (board.getColumnSize() >= 4) {
			Player winner = null;
			
			for (int i = 0; i < board.getColumnSize()-3; i++) {
				winner = checkLToRDiag(board, bluePlayer, redPlayer, i, board.getRowSize()-1);
				if (winner != null) return winner;
			}
			
			if (board.getRowSize() > 4) {
				for (int i = board.getRowSize()-1; i > 3; i--) {
					winner = checkLToRDiag(board, bluePlayer, redPlayer, 0, i);
					if (winner != null) return winner;
				}
			}
		}
		
		return null;
	}
	
	public static Player checkLToRDiag(Board board, Player bluePlayer, Player redPlayer, int startColumn, int startRow) {
		Square lastTokenColor = Square.Empty;
		int connectedCount = 0;
		
		int i = startRow;
		for (int j = startColumn; j < board.getColumnSize() && i >= 0; j++) {
			Square color = board.getColumn(j).getSquare(i);
			if (color == Square.Empty) {
				connectedCount = 0;
			} else if (lastTokenColor != color) {
				connectedCount = 1;
			} else {
				connectedCount++;
			}
			lastTokenColor = color;
			if (connectedCount == 4) {
				return color == Square.Blue ? bluePlayer : redPlayer;
			}
			i--;
		}
		
		return null;
	}
	
}
