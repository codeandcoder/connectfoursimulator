package net.codeandcoder.connectfour.utils;

import java.util.Random;

public class RandomUtils {

	private static RandomUtils singleton;
	private Random random;
	
	private RandomUtils() {
		this.random = new Random();
	}
	
	public static RandomUtils getInstance() {
		if (singleton == null) {
			singleton = new RandomUtils();
		}
		
		return singleton;
	}
	
	public Random getRandom() {
		return random;
	}
	
}
