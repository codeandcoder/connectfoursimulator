package net.codeandcoder.connectfour.simulator;

import net.codeandcoder.connectfour.board.Board;
import net.codeandcoder.connectfour.board.Board.Square;
import net.codeandcoder.connectfour.board.BoardBuilder;
import net.codeandcoder.connectfour.interfaces.Player;
import net.codeandcoder.connectfour.interfaces.Simulator;
import net.codeandcoder.connectfour.interfaces.UIListener;
import net.codeandcoder.connectfour.ui.SimpleConsoleUI;
import net.codeandcoder.connectfour.utils.GameUtils;
import net.codeandcoder.connectfour.utils.RandomUtils;

public class ConnectFourSimulator implements Simulator {
	
	private static final int MIN_COLUMNS = 5;
	private static final int MAX_COLUMNS = 15;
	private static final int MIN_ROWS = 4;
	private static final int MAX_ROWS = 13;
	private UIListener uiListener;
	
	public ConnectFourSimulator() {
		uiListener = new SimpleConsoleUI();
	}
	
	public ConnectFourSimulator(UIListener listener) {
		uiListener = listener;
	}
	
	@Override
	public Result simulateOneGame(Player bluePlayer, Player redPlayer) {
		int columns = RandomUtils.getInstance().getRandom().nextInt(MAX_COLUMNS - MIN_COLUMNS) + MIN_COLUMNS;
		int rows = RandomUtils.getInstance().getRandom().nextInt(MAX_ROWS - MIN_ROWS) + MIN_ROWS;
		return simulateOneGame(bluePlayer, redPlayer, columns, rows, true);
	}
	
	@Override
	public Result simulateOneGame(Player bluePlayer, Player redPlayer,
			int columns, int rows) {
		return simulateOneGame(bluePlayer, redPlayer, columns, rows, true);
	}

	public Result simulateOneGame(Player bluePlayer, Player redPlayer, int columns, int rows, boolean individualLogging) {
		BoardBuilder boardBuilder = new BoardBuilder(columns, rows);
		Player currentPlayer = bluePlayer;
		Result result = new Result();
		result.player1 = bluePlayer;
		result.player2 = redPlayer;
		Board board = null;
		
		try {
			bluePlayer.init();
		} catch (Exception ex) {
			result.winner = bluePlayer;
			ex.printStackTrace();
		}
		
		try {
			redPlayer.init();
		} catch (Exception ex) {
			result.winner = redPlayer;
			ex.printStackTrace();
		}
		
		try {
			board = boardBuilder.buildBoard();
			if (individualLogging) {
				uiListener.gameStarts(board, bluePlayer, redPlayer);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		result.moves = 0;
		while (result.winner == null && !boardBuilder.isFull()) {
			try {
				Square color = currentPlayer.equals(bluePlayer) ? Square.Blue : Square.Red;
				int column = currentPlayer.act(color, board);
				boardBuilder.putToken(column, color);
				board = boardBuilder.buildBoard();
				if (individualLogging) {
					uiListener.updateBoard(board);
				}
				result.winner = GameUtils.checkWin(board, bluePlayer, redPlayer);
				currentPlayer = currentPlayer.equals(bluePlayer) ? redPlayer : bluePlayer;
				result.moves++;
			} catch (Exception ex) {
				result.winner = currentPlayer.equals(bluePlayer) ? redPlayer : bluePlayer;
				ex.printStackTrace();
			}
		}
		
		uiListener.gameFinished(result.winner, result.moves);
		
		return result;
	}
	
	@Override
	public Stats simulateMultipleRounds(Player player1, Player player2,
			int numberOfRounds) {
		
		Stats stats = new Stats();
		stats.player1 = player1;
		stats.player2 = player2;
		
		int player1Moves = 0;
		int player2Moves = 0;
		
		for (int i = 0; i < numberOfRounds; i++) {
			Player blue = i % 2 == 0 ? player1 : player2;
			Player red = i % 2 == 0 ? player2 : player1;
			int columns = RandomUtils.getInstance().getRandom().nextInt(MAX_COLUMNS - MIN_COLUMNS) + MIN_COLUMNS;
			int rows = RandomUtils.getInstance().getRandom().nextInt(MAX_ROWS - MIN_ROWS) + MIN_ROWS;
			Result result = simulateOnRound(blue, red, columns, rows, false);
			if (result.winner == null) {
				stats.draws++;
			} else if (result.winner.equals(player1)) {
				stats.player1Wins++;
				player1Moves += result.moves;
			} else {
				stats.player2Wins++;
				player2Moves += result.moves;
			}
		}
		
		stats.player1MovesPerWin = stats.player1Wins != 0 ? player1Moves / (stats.player1Wins*2) : 0;
		stats.player2MovesPerWin = stats.player2Wins != 0 ? player2Moves / (stats.player2Wins*2) : 0;
		
		uiListener.showStats(stats);
		
		return stats;
	}

	@Override
	public Result simulateOnRound(Player player1, Player player2) {
		int columns = RandomUtils.getInstance().getRandom().nextInt(MAX_COLUMNS - MIN_COLUMNS) + MIN_COLUMNS;
		int rows = RandomUtils.getInstance().getRandom().nextInt(MAX_ROWS - MIN_ROWS) + MIN_ROWS;
		return simulateOnRound(player1, player2, columns, rows, true);
	}
	
	@Override
	public Result simulateOnRound(Player player1, Player player2, int columns,
			int rows) {
		return simulateOnRound(player1, player2, columns, rows, true);
	}
	
	public Result simulateOnRound(Player player1, Player player2, int columns, int rows, boolean individualLogging) {
		Result resume = new Result();
		resume.player1 = player1;
		resume.player2 = player2;
		
		Result round1 = simulateOneGame(player1, player2, columns, rows, individualLogging);
		resume.moves += round1.moves;
		Result round2 = simulateOneGame(player2, player1, columns, rows, individualLogging);
		resume.moves += round2.moves;
			
		if (round1.winner == null) {
			if (round2.winner != null) {
				resume.winner = round2.winner;
			}
		} else if (round2.winner == null) {
			if (round1.winner != null) {
				resume.winner = round1.winner;
			}
		} else {
			resume.winner = round1.winner.equals(round2.winner) ? round1.winner : null;
		}
		
		return resume;
	}
	
}
