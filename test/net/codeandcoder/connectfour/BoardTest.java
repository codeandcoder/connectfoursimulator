package net.codeandcoder.connectfour;

import static org.junit.Assert.*;
import net.codeandcoder.connectfour.board.Board;
import net.codeandcoder.connectfour.board.Board.Square;
import net.codeandcoder.connectfour.board.BoardBuilder;

import org.junit.Test;

public class BoardTest {

	private BoardBuilder boardBuilder = new BoardBuilder(7,6);
	
	@Test(expected=Exception.class)
	public void incorrectTokenBelow() throws Exception {
		boardBuilder.putToken(-1, Square.Blue);
	}
	
	@Test(expected=Exception.class)
	public void incorrectTokenAbove() throws Exception {
		boardBuilder.putToken(boardBuilder.getColumnSize(), Square.Blue);
	}
	
	@Test
	public void correctPut() throws Exception {
		boardBuilder.putToken(1, Square.Blue);
		assertEquals(Square.Blue, boardBuilder.buildBoard().getColumn(1).getSquare(0));
	}

}
