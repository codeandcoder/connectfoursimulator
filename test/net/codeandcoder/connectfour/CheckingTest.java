package net.codeandcoder.connectfour;

import static org.junit.Assert.*;
import net.codeandcoder.connectfour.ai.DumbPlayer;
import net.codeandcoder.connectfour.board.Board;
import net.codeandcoder.connectfour.board.BoardBuilder;
import net.codeandcoder.connectfour.board.Board.Square;
import net.codeandcoder.connectfour.interfaces.Player;
import net.codeandcoder.connectfour.utils.GameUtils;

import org.junit.Test;

public class CheckingTest {

	@Test
	public void horizontalCheck() {
		BoardBuilder boardBuilder = new BoardBuilder(7,6);
		try {
			boardBuilder.putToken(0, Square.Blue);
			boardBuilder.putToken(1, Square.Blue);
			boardBuilder.putToken(2, Square.Blue);
			boardBuilder.putToken(3, Square.Blue);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Player bluePlayer = new DumbPlayer();
		Player redPlayer = new DumbPlayer();
		
		try {
			Player winner = GameUtils.checkHorizontal(boardBuilder.buildBoard(), bluePlayer, redPlayer);
			assertEquals(bluePlayer, winner);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void verticalCheck() {
		BoardBuilder boardBuilder = new BoardBuilder(7,6);
		try {
			boardBuilder.putToken(0, Square.Blue);
			boardBuilder.putToken(0, Square.Blue);
			boardBuilder.putToken(0, Square.Blue);
			boardBuilder.putToken(0, Square.Blue);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Player bluePlayer = new DumbPlayer();
		Player redPlayer = new DumbPlayer();
		
		try {
			Player winner = GameUtils.checkVertical(boardBuilder.buildBoard(), bluePlayer, redPlayer);
			assertEquals(bluePlayer, winner);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void LToRDiagCheck() {
		BoardBuilder boardBuilder = new BoardBuilder(7,6);
		try {
			boardBuilder.putToken(0, Square.Blue);
			boardBuilder.putToken(0, Square.Red);
			boardBuilder.putToken(0, Square.Blue);
			boardBuilder.putToken(0, Square.Blue);
			boardBuilder.putToken(0, Square.Blue);
			
			boardBuilder.putToken(1, Square.Blue);
			boardBuilder.putToken(1, Square.Red);
			boardBuilder.putToken(1, Square.Blue);
			boardBuilder.putToken(1, Square.Blue);
			
			boardBuilder.putToken(2, Square.Blue);
			boardBuilder.putToken(2, Square.Red);
			boardBuilder.putToken(2, Square.Blue);
			
			boardBuilder.putToken(3, Square.Blue);
			boardBuilder.putToken(3, Square.Blue);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Player bluePlayer = new DumbPlayer();
		Player redPlayer = new DumbPlayer();
		
		try {
			Player winner = GameUtils.checkLeftToRightDiagonals(boardBuilder.buildBoard(), bluePlayer, redPlayer);
			assertEquals(bluePlayer, winner);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void RToLDiagCheck() {
		BoardBuilder boardBuilder = new BoardBuilder(7,6);
		try {
			boardBuilder.putToken(boardBuilder.getColumnSize()-1, Square.Blue);
			boardBuilder.putToken(boardBuilder.getColumnSize()-1, Square.Red);
			boardBuilder.putToken(boardBuilder.getColumnSize()-1, Square.Blue);
			boardBuilder.putToken(boardBuilder.getColumnSize()-1, Square.Blue);
			boardBuilder.putToken(boardBuilder.getColumnSize()-1, Square.Blue);
			
			boardBuilder.putToken(boardBuilder.getColumnSize()-2, Square.Blue);
			boardBuilder.putToken(boardBuilder.getColumnSize()-2, Square.Red);
			boardBuilder.putToken(boardBuilder.getColumnSize()-2, Square.Blue);
			boardBuilder.putToken(boardBuilder.getColumnSize()-2, Square.Blue);
			
			boardBuilder.putToken(boardBuilder.getColumnSize()-3, Square.Blue);
			boardBuilder.putToken(boardBuilder.getColumnSize()-3, Square.Red);
			boardBuilder.putToken(boardBuilder.getColumnSize()-3, Square.Blue);
			
			boardBuilder.putToken(boardBuilder.getColumnSize()-4, Square.Blue);
			boardBuilder.putToken(boardBuilder.getColumnSize()-4, Square.Blue);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Player bluePlayer = new DumbPlayer();
		Player redPlayer = new DumbPlayer();
		
		try {
			Player winner = GameUtils.checkRightToLeftDiagonals(boardBuilder.buildBoard(), bluePlayer, redPlayer);
			assertEquals(bluePlayer, winner);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
